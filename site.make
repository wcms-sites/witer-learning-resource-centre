core = 7.x
api = 2

; uw_feed_witer_blog
projects[uw_feed_witer_blog][type] = "module"
projects[uw_feed_witer_blog][download][type] = "git"
projects[uw_feed_witer_blog][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_witer_blog.git"
projects[uw_feed_witer_blog][download][tag] = "7.x-1.0"
projects[uw_feed_witer_blog][subdir] = ""

; uw_lib_primo_search
projects[uw_lib_primo_search][type] = "module"
projects[uw_lib_primo_search][download][type] = "git"
projects[uw_lib_primo_search][download][url] = "https://git.uwaterloo.ca/library/uw_lib_primo_search.git"
projects[uw_lib_primo_search][download][tag] = "7.x-2.0"
projects[uw_lib_primo_search][subdir] = ""
